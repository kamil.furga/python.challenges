import random

heads = 0
tails = 0
for i in range(50):
    if heads >= 1 and tails >= 1:
        break
    else:
        if random.randint(0, 1) == 0:
            heads = heads + 1
            print("heads")
        else:
            tails = tails + 1
            print("tails")
