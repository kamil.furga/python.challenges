import random

candidate_a_region1 = 0
candidate_a_region2 = 0
candidate_a_region3 = 0

for i in range(1000):
    if random.randint(1, 100) >= 50.1:
        candidate_a_region1 = candidate_a_region1 + 1
    if random.randint(1, 100) >= 50.1:
        candidate_a_region2 = candidate_a_region2 + 1
    if random.randint(1, 100) >= 50.1:
        candidate_a_region3 = candidate_a_region3 + 1

print(f"Region 1: {candidate_a_region1/10}% of winning\n"
      f"Region 2: {candidate_a_region2/10}% of winning\n"
      f"Region 3: {candidate_a_region3/10}% of winning")
