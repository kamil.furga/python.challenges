def invest(amount, rate, years):
    for year in range(1, years + 1):
        amount = amount * (1 + rate)
        print(f"Year {year}: ${amount:.2f}")


amount_of_investment = float(input("specify the amount of investment:"))
rate_of_investment = float(input("specify the rate of investment:"))
years_of_investment = int(input("specify years of investment:"))

invest(amount_of_investment, rate_of_investment, years_of_investment)
