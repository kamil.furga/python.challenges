def convert_cel_to_far(x):
    y: float = float(x) * 9 / 5 + 32
    return float(y)


def convert_far_to_cel(x):
    y: float = (float(x) - 32 ) * 5 / 9
    return float(y)


degrees_in_fahrenheit = input("Enter a temperature in degrees F:")
celcius_degrees = convert_far_to_cel(degrees_in_fahrenheit)
print(f"{degrees_in_fahrenheit} degrees F = {celcius_degrees:.2f} degrees C")

degrees_in_celcius = input("Enter a temperature in degrees C:")
fahrenheit_degrees = convert_cel_to_far(degrees_in_celcius)
print(f"{degrees_in_celcius} degrees C = {fahrenheit_degrees:.2f} degrees F")
